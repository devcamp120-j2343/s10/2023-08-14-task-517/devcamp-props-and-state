import { Component } from "react";

class Info extends Component {
    render() {
        const { firstName, lastName, favNumber, children } = this.props;

        console.log(children);

        return (
            <div>
                <p>My name is {lastName} {firstName} and my favourite number is {favNumber}.</p>
            </div>
        )
    }
}

export default Info;